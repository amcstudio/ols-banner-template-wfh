var gulp = require('gulp'),
plumber = require('gulp-plumber'),
fs = require('fs'),
useref = require('gulp-useref');

var datajson=JSON.parse(fs.readFileSync('./data.json')),
serverPath =datajson.wfh.serverPath,
remoteWork =datajson.wfh.remoteWork;


  /*--------------------------------------------------
  MOVE ALL ASSETS INTO THE DEPLOY FOLDER
  --------------------------------------------------*/

module.exports = {

    moveFiles : function (){
      if(remoteWork === 'y'){
        return gulp.src(["src/*", 'src/img/*', 'src/fonts/*', 'src/css/fonts/*', 'src/css/*', '!src/scss/', '!src/index.html'], {
          base: './src',
          nodir: true
        })
        .pipe(plumber())
        .pipe(gulp.dest('deploy'))
        .pipe(gulp.dest(serverPath+'/remoteMirror'));
      }
      else{
        return gulp.src(["src/*", 'src/img/*', 'src/fonts/*', 'src/css/fonts/*', 'src/css/*', '!src/scss/', '!src/index.html'], {
          base: './src',
          nodir: true
        })
        .pipe(plumber())
        .pipe(gulp.dest('deploy'));
      }
        
    },

    moveHTML : function (){
      if(remoteWork === 'y'){
        return gulp.src('src/index.html')
        .pipe(useref())
        .pipe(gulp.dest('deploy'))
        .pipe(gulp.dest(serverPath+'/remoteMirror'));
      }
      else{
        return gulp.src('src/index.html')
        .pipe(useref())
        .pipe(gulp.dest('deploy'));
      }
      
    }
    
}