var gulp = require('gulp'), 
    confirm = require('gulp-confirm'),
    jsonModify = require('gulp-json-modify'),
    fs = require('fs'),
    datajson=JSON.parse(fs.readFileSync('./data.json')),
    serverPath =datajson.wfh.serverPath,
    remoteWork='n';


  module.exports = function () {

      json = JSON.parse(fs.readFileSync('./package.json'))
      return gulp.src('src/**/*')
        .pipe(confirm({
          question: 'working from home? Press | yes: y | no: n ',
          proceed: function (answer) {
           
            remoteWork = answer ;
            remoteModify();
            // remoteMirrorStream();
            return true;
          }
        }))
   
  };



function remoteModify(){
    return gulp.src('./data.json')
    .pipe(jsonModify({ key: 'wfh.remoteWork', value: remoteWork }))
      .pipe(gulp.dest(serverPath+'/remoteMirror'))
}

// function remoteMirrorStream(){
//   return gulp.src('./')
 
//     .pipe(gulp.dest(serverPath+'/remoteMirror'))
// }

